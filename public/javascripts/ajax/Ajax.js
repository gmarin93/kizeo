var Resource = function (resource_name) {

    this.resource_name = resource_name;
    
    this.cedulaID = function (id) {
        return $.getJSON({ url: this.url('/_resource_/cedulas/_id_',id), type:'GET'});
    };
   

    this.getId = function () {
        return $.getJSON({ url: this.url('/_resource_/'), type:'GET'});
    };
   
    this.getData = function (id) {
        return $.getJSON({ url: this.url('/_resource_/getData/_id_',id), type:'GET'});
    };

  
	this.url = function(url, id) {
		return url.replace('_resource_', this.resource_name).replace('_id_', id);
	};
	

	
};
