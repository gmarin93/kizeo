var Index = new Resource('index');

// const getId = async () => {
// 	debugger
// 	var request = await Index.getId();
// 	// var id = request.id;
// 	var forms=request.forms;

// 	return forms;
// };

const Forms = async () => {
	debugger
	var request = await Index.getId();
	// var id = request.id;
	var forms=request.forms;

	return forms;
};

// const getData = async () => {
// 	debugger
// 	// var id = await getId();
// 	var forms= await Forms();
// 	if(Object.keys(forms)) {
// 		forms.map(async e=>{
// 			debugger
// 			var data = await Index.getData(e.id);
// 			// console.log(`Formularios: ${JSON.stringify(data.data.data)}`);
// 			data.data.data.map(async subE=>{
// 				debugger
// 				var e=subE.fields;
// 				if(e.cedula){
// 					var cedulas= await Index.cedulaID(parseInt(e.cedula.value));
// 					// console.log(cedulas);
					
// 				}
// 			})

// 		}

// 		)}
// 	// var data = await Index.getData(id);
// 	// await createTable(data);
// };

const clearData = (data) => {
	
	var array = [];

	data.data.data.map(function(item) {
		
		item=item.fields;
		array.push({
			Cedula:item.cedula.value,
			Lote:item.lote.value, 
			block:item.block.value,
			Fecha:item.fecha_y_hora.value,
			area:item.area.value,
			calibres:item.calibres.value,
			conica:item.conica.value,
			corcho:item.corcho.value,
			craw:item.craw.value,
			Dano_sol:item.d_sol.value,
			Dano_roedor:item.dano_roedor.value,
			Desbraquea:item.desbraq.value,
			Fallas:item.fallas.value,
			Fruta_atrasada:item.fruta_atrazada.value,
			Gomosis:item.gomosis.value,
			Gps_final_latitude:item.gps_final_latitude.value,
			Gps_final_longitude:item.gps_final_longitude.value,
			Gps_inicial_latitude:item.gps_inicial_latitude.value, 
			Gps_inicial_longitude:item.gps_inicial_longitude.value,
			Hueco:item.hueco.value,
			Menos:item.menos.value,
			Motivos_Rechazo:item.motivos_rechazo.value,
			Muestras:item.muestras.value,
			Natural :item.nat.value,
			Otros:item.otros.value,
			s:item.s.value,
			s1:item.s1.value,
			s2:item.s2.value,
			s3:item.s3.value,
			s4:item.s4.value,
			s5:item.s5.value,
			s6:item.s6.value,
			Semana_estim:item.semana_estim.value,
			Sombra:item.sombra.value	
		});
	});

	return array;
};

const createTable = async (data) => {
	debugger
	var count = 0;
    var info = await clearData(data);
    var table = $('#data');
    $('#data tr:not(:first)').remove();
	info.forEach(function(json) {
		count++;
		table.append(
				`<tr>
				<th scope=row>
				${json.Cedula}
				</th>
				 <th scope=row>
				 ${json.Lote}
				 </th>
				 <th scope=row>
				 ${json.block}
				 </th>
				 <th scope=row>
				 ${json.Fecha}
				 </th>
				 <th scope=row>
				 ${json.area}
				 </th>
				 <th scope=row>
				 ${json.calibres}
				 </th>
				 <th scope=row>
				 ${json.conica}
				 </th>
				 <th scope=row>
				 ${json.corcho}
				 </th>
				 <th scope=row>
				 ${json.craw}
				 </th>
				 <th scope=row>
				 ${json.Dano_sol}
				 </th>
				 <th scope=row>
				 ${json.Dano_roedor}
				 </th>
				 <th scope=row>
				 ${json.Desbraquea}
				 </th>
				 <th scope=row>
				 ${json.Fallas}
				 </th>
				 <th scope=row>
				 ${json.Fruta_atrasada}
				 </th>
				 <th scope=row>
				 ${json.Gomosis}
				 </th>
				 <th scope=row>
				 ${json.Gps_final_latitude}
				 </th>
				 <th scope=row>
				 ${json.Gps_final_longitude}
				 </th>
				 <th scope=row>
				 ${json.Gps_inicial_latitude}
				 </th>
				 <th scope=row>
				 ${json.Gps_inicial_longitude}
				 </th>
				 <th scope=row>
				 ${json.Hueco}
				 </th>
				 <th scope=row>
				 ${json.Menos}
				 </th>
				 <th scope=row>
				 ${json.Motivos_Rechazo}
				 </th>
				 <th scope=row>
				 ${json.Muestras}
				 </th>
				 <th scope=row>
				 ${json.Natural}
				 </th>
				 <th scope=row>
				 ${json.Otros}
				 </th>
				 <th scope=row>
				 ${json.s}
				 </th>
				 <th scope=row>
				 ${json.s1}
				 </th>
				 <th scope=row>
				 ${json.s2}
				 </th>
				 <th scope=row>
				 ${json.s3}
				 </th>
				 <th scope=row>
				 ${json.s4}
				 </th>
				 <th scope=row>
				 ${json.s5}
				 </th>
				 <th scope=row>
				 ${json.s6}
				 </th>
				 <th scope=row>
				 ${json.Semana}
				 </th>
				 <th scope=row>
				 ${json.Sombra}
				 </th>
				 </tr>
				`

		);
	});
};

$(document).ready(function() {
	// setInterval(function() {
		getData();
	// }, 3000);
});
