var express = require('express');
var router = express.Router();
var request = require('request');
var reqAsync = require('async-request');
var models = require('../models');
var sequelize = require('sequelize');
const Op = sequelize.Op;
var app = express();

/** 
 *Une todos los defectos de fruta y los agrupa en un objeto literal para luego retornar un arreglo 
 * @param {elemento del arreglo de datos de kizeo} e 
 * @param {arreglo que contiene los defectos y calibres que corresponden a los datos de kizeo} array 
 */
function unirDefectosDatos(e, array) {
	var array_attr = [
		{ semanaestimada: e.semana_estim, value: e.menos, objeto: e.menos, kizeo: "menos" }, { value: e.hueco, objeto: e.hueco, kizeo: "hueco" }, { value: e.fallas, objeto: e.fallas, kizeo: "fallas" },
		{ semanaestimada: e.semana_estim, value: e.gomosis, objeto: e.gomosis, kizeo: "gomosis" }, { value: e.corcho, objeto: e.corcho, kizeo: "corcho" }, { value: e.d_sol, objeto: e.d_sol, kizeo: "d_sol" },
		{ semanaestimada: e.semana_estim, value: e.desbraq, objeto: e.desbraq, kizeo: "desbraq" }, { value: e.fruta_atrazada, objeto: e.fruta_atrazada, kizeo: "fruta_atrazada" },
		{ semanaestimada: e.semana_estim, value: e.conica, objeto: e.conica, kizeo: "conica" }, { value: e.sombra, objeto: e.sombra, kizeo: "sombra" }, { value: e.dano_roedor, objeto: e.dano_roedor, kizeo: "dano_roedor" },
		{ semanaestimada: e.semana_estim, value: e.otros, objeto: e.otros, kizeo: "otros" }, { value: e.nat, objeto: e.nat, kizeo: "nat" }, { value: e.cochinilla, objeto: e.cochinilla, kizeo: "cochinilla" },
		{ value: e.techla, objeto: e.techla, kizeo: "techla" }];

//{ semanaestimada: e.semana_estim, value: e.craw, objeto: e.craw, kizeo: "craw" },

	var objectD = [];
	var array_def = [];

	array_attr.map((element) => {

		if (element.objeto) {
			objectD = array.filter(x => x.kizeodefecto === element.kizeo)
			valor = { codigoDefecto: objectD[0].codigo, kizeodefecto: element.kizeo, value: element.value, semanaestimada: element.semanaestimada }
			array_def.push(valor);
			valor = {};
			objectD = [];
		}
	})

	return array_def;
}

/**
 * Une todos los calibres con respecto a los que tienen relacion con kizeo
 * @param {*} e 
 * @param {*} array 
 */

function unirCalibresDatos(e, array) {

	var valor ={};
	var array_cal = [];
	var objectS = "";
	var Numcedula = "";
	var arraysplit = e.cedula1.value === '' ? [] : e.cedula1.split('*'); //Se obtiene en un arreglo la cedula seleccionada en el combo del formulario de kizeo
	var subarray = arraysplit.length > 0 ? arraysplit[1].split("=") : []; //Se obtiene el número de cédula

	if (subarray.length > 0) //Sí se seleccionó la cédulas desde el combo, entra
		Numcedula = subarray[1].trim();
	else
		Numcedula = e.cedula.value


	var defectos = {
		defectosFruta: unirDefectosDatos(e, array),
		cedula: Numcedula
	};

	var array_attr = [
		{ objeto: e.s, valor: e.s, kizeocalibre: 's', cedula: Numcedula, semanaestimada: e.semana_estim },
		{ objeto: e.s1, valor: e.s1, kizeocalibre: 's1', cedula: Numcedula, semanaestimada: e.semana_estim },
		{ objeto: e.s2, valor: e.s2, kizeocalibre: 's2', cedula: Numcedula, semanaestimada: e.semana_estim },
		{ objeto: e.s3, valor: e.s3, kizeocalibre: 's3', cedula: Numcedula, semanaestimada: e.semana_estim },
		{ objeto: e.s4, valor: e.s4, kizeocalibre: 's4', cedula: Numcedula, semanaestimada: e.semana_estim },
		{ objeto: e.s5, valor: e.s5, kizeocalibre: 's5', cedula: Numcedula, semanaestimada: e.semana_estim },
		{ objeto: e.s6, valor: e.s6, kizeocalibre: 's6', cedula: Numcedula, semanaestimada: e.semana_estim },
		{ objeto: e.sl, valor: e.sl, kizeocalibre: 'sl', cedula: Numcedula, semanaestimada: e.semana_estim },
		{ objeto: e.sl1, valor: e.sl1, kizeocalibre: 'sl1', cedula: Numcedula, semanaestimada: e.semana_estim },
		{ objeto: e.sl2, valor: e.sl2, kizeocalibre: 'sl2', cedula: Numcedula, semanaestimada: e.semana_estim },
		{ objeto: e.sl3, valor: e.sl3, kizeocalibre: 'sl3', cedula: Numcedula, semanaestimada: e.semana_estim },
		{ objeto: e.sl4, valor: e.sl4, kizeocalibre: 'sl4', cedula: Numcedula, semanaestimada: e.semana_estim }
	];

	array_attr.map(element=>{

		objectS=array.filter(x=>String(x.kizeocalibre)===String(element.kizeocalibre));
		
		if(objectS[0]){

			valor={
				valor:element.valor,
				kizeocalibre:element.kizeocalibre,
				calibreid:objectS[0].calibreid,
				cedula:element.cedula,
				semanaestimada:element.semanaestimada
			};

			array_cal.push(valor);

			valor={};
		}
	});

	array_cal.push(defectos);

	console.log(array_cal);

	return array_cal;
}

/**
 * Busca las cédulas de premaduración, las actualiza a ejecutadas(estadoid=3) y devuelve el cedulamuestreopremaid
 * @param {cedula capurado del formulario de kizeo} cedula 
 */

async function buscarCedulaPrema(e) {

	var cedulamuestreopremaid = 0;
	var cedula = e.cedula.value;

	try {

		await models.prd_cedulaaplicacion.findOne({
			where: { numerocedula: cedula },
			include: [{
				model: models.prd_cedulamuestreoprema, required: true,
				attributes: ['cedulamuestreopremaid']
			}],
			order: [
				['cedulaaplicacionid', 'ASC']]
		}).then(alerta => {
			cedulamuestreopremaid = alerta.prd_cedulamuestreoprema.dataValues.cedulamuestreopremaid;//.cedulamuestreopremaid;
			alerta.update({
				estadoid: 3
			})
		})

	} catch (error) {
		cedulamuestreopremaid = false;
	}

	return cedulamuestreopremaid;
}

/**
 * Actualiza la cantidad de cochinilla encontrada
 * @param {*} interna 
 * @param {*} externa 
 * @param {*} cedulapremaid 
 */
async function actualizarCochinilla(interna, externa, cedulapremaid) {

	try {

		await models.prd_cedulamuestreoprema.findOne({
			where: { cedulamuestreopremaid: cedulapremaid },

		}).then(alerta => {
			alerta.update({
				totalcochinillaexterna: externa,
				totalcochinillainterna: interna
			})
		})

	} catch (error) {
		console.log(error);
	}
}



/**
 * Almacena las coordenadas de donde fueron capturados los muestreos
 * @param {id de la cedula de muestreo de estimacion} cedulamuestreocalibreid 
 * @param {campo proveniente del formulario de Kizeo} e 
 */

async function grd_coordenadasEstimacion(cedulamuestreocalibreid, e) {

	var gpsinicial = e.gps_inicial_latitude.value
	var gpsfinal = e.gps_inicial_longitude.value;

	try {
		await models.prd_cedulamuestreocalibre.findOne({
			where: { cedulamuestreocalibreid: cedulamuestreocalibreid }
		}).then(cedulamuestreo => {
			cedulamuestreo.update({
				gpsinicial: gpsinicial,
				gpsfinal: gpsfinal
			})
		})
	} catch (error) {
		console.log(error);
	}
}

/**
 * Almacena las coordenadas de donde fueron capturados los muestreos
 * @param {id de la cedula de muestreo de estimacion} cedulamuestreocalibreid 
 * @param {campo proveniente del formulario de Kizeo} e 
 */
async function grd_coordenadasPremaduracion(cedulamuestreopremaid, e) {

	var gpsinicial = e.gps_inicial_latitude.value
	var gpsfinal = e.gps_inicial_longitude.value;

	try {
		await models.prd_cedulamuestreoprema.findOne({
			where: { cedulamuestreopremaid: cedulamuestreopremaid }
		}).then(cedulamuestreo => {
			cedulamuestreo.update({
				gpsinicial: gpsinicial,
				gpsfinal: gpsfinal
			})
		})
	} catch (error) {
		console.log(error);
	}
}

/**
 * Busca sí las cédulas de estimación o premaduración ya han sido ingresadas y devuelve un boolean de si existen datos y un id ya sea de cedulamuestreopremaid o cedulamuestreocalibreid
 * @param {cedulamuestreid de la cédula que se está ejecutando en ese momento} cedulamuestreoid
 * * @param {tipo de datos a ingresar(Estimación o premaduración)} tipo 
 */
async function BuscarDetallesCedula(cedula, tipo) {

	var conDatos = false;
	var detalleCed = Object();
	var cedulamuestreocalibreid = await buscarCedula(cedula, tipo);
	detalleCed.cedulamuestreocalibreid = cedulamuestreocalibreid;

	try {

		if (cedulamuestreocalibreid) {

			switch (tipo) {
				case "ESTIMACION":

					await models.prd_cedulamuestreocalibre.count({
						where: { cedulamuestreocalibreid: cedulamuestreocalibreid },
						include: [{
							model: models.prd_cedmuestreocaldet, required: true
						}, {
							model: models.prd_cedmuestreocalrec, required: true
						}],
					})
						.then(async alerta => {
							if (parseInt(alerta) >= 112) //Mayor ó igual a 112 porque son 7 registros de detalle muestreo y 16 de defectos muestreo, con este inner join se multiplican los datos y cuando la informacioón está completa, deberían haber 112 registros
								conDatos = true;
							else if (parseInt(alerta) < 112 && parseInt(alerta) > 0) {
								try {
									await models.prd_cedmuestreocaldet.destroy({
										where: { cedulamuestreocalibreid: cedulamuestreocalibreid }
									}),
										await models.prd_cedmuestreocalrec.destroy({
											where: { cedulamuestreocalibreid: cedulamuestreocalibreid }
										})
								} catch (error) {
									console.log(error);
								}
							}
						})
					break;

				case "PREMADURACION":
					await models.prd_cedmuestreopremadet.count({
						where: { cedulamuestreopremaid: cedulamuestreocalibreid }
					}).then(async alerta => {
						if (parseInt(alerta) > 0)
							conDatos = true;
					})
					break;

				default:
					break;
			}

			detalleCed.conDatos = conDatos;
		}
		else {
			detalleCed.conDatos = false;
			detalleCed.cedulamuestreocalibreid = false;
		}

	} catch (error) {
		console.log(error + "La cedula es: " + cedula);
	}

	return detalleCed;
}

/**
 * Busca las cédulas de estimación que ya han sido ingresada y actualiza sus nuevos datos
 * @param {cedulamuestreid de la cédula que se está ejecutando en ese momento} cedulamuestreoid
 * * @param {tipo de datos a ingresar(Estimación o premaduración)} tipo 
 */
async function actualizarDetallesCedula(fields, cedulamuestreoid, tipo) {

	var conDatos = false;

	try {

		switch (tipo) {
			case "ESTIMACION":
				if (fields.calibreid) {
					await models.prd_cedmuestreocaldet.findOne({
						where: { cedulamuestreocalibreid: cedulamuestreoid, calibreid: fields.calibreid }
					}).then(async alerta => {
						if(alerta){
							await alerta.update({
								cantidad: fields.valor,
								semanaestimada: fields.semanaestimada
							}).then(a => {
								conDatos = true
								console.log(`Cédula ${cedulamuestreoid} actualizada`);
							}).catch(e => {
								console.log(`Cédula ${cedulamuestreoid} no ha sido actualizada ${e}`);
							})
						}

					})
				}
				break;
			case "PREMADURACION":
				await models.prd_cedmuestreopremadet.findOne({
					where: { cedulamuestreopremaid: cedulamuestreoid, calibreid: fields.calibreid }
				}).then(async alerta => {
					if (alerta > 0)
						conDatos = true;

					await alerta.update({
						brix: fields.brix,
						translucidez: fields.translucidez
					}).then(a => {
						console.log(`Cédula ${cedulamuestreoid} actualizada`);
					}).catch(e => {
						console.log(`Cédula ${cedulamuestreoid} no ha sido actualizada ${e}`);
					}
					)
				})
				break;

			default:
				break;
		}

	} catch (error) {

		console.log(error);
	}

	return conDatos;
}

/**
 * Obtiene la lista de cédulas de estimación y actualiza los números de cédulas en la lista "CEDULAS"(Formulario Kizeo)
 */
async function obtenerListasExternas() {

	var listas = "";
	const token = await getTokenAPI(); //Se obtiene el token de acceso
	var items = {};
	var idForm = 0;
	var cedulasData = "";
	var muestras = [];

	try {

		listas = await reqAsync('https://www.kizeoforms.com/rest/v3/lists', {
			method: 'GET',
			headers: token
		});

		//Se obtienen las cédulas a muestrear ya asignadas al grupo de muestreo	
		cedulasData = await ObtenerCedulaDatos();

		//Se crea el objeto cedula con el dato listo para añadir a la lista
		cedulasData.map(e => {
			var cedulaObject =
				` *Cedula= ${e.dataValues.numerocedula} 
				*Lote= ${e.dataValues.interface.dataValues.ciclo.dataValues.bloques.dataValues.lotes.dataValues.numerolote}
				*Bloque= ${e.dataValues.interface.dataValues.ciclo.dataValues.bloques.dataValues.numerobloque} 
				*Grupo=${e.dataValues.interface.dataValues.ciclo.dataValues.grupodeforza}
				*Muestras= ${Math.round((parseInt(e.dataValues.interface.dataValues.ciclo.dataValues.bloques.dataValues.totalsemillas) * 0.02).toFixed(2))}
				`
			muestras.push(cedulaObject)
		})

		items = {
			"items": muestras
		}

		listas = (JSON.parse(listas.body));
		listas = listas.lists.filter(e => e.name === "estimacion");
		idForm = listas[0].id; //Se obtiene el id de la lista externa

		var options = {
			url: `https://www.kizeoforms.com/rest/v3/lists/${idForm}`,
			method: 'PUT',
			headers: token,
			body: JSON.stringify(items)
		};

		await request(options, function (err, req, body) { console.log(body); })

	} catch (error) {
		console.log(error);
	}
}

/**
 * Obtiene datos del Lote y Bloque a muestrear, así como su grupo de forza
 */
async function ObtenerCedulaDatos() {

	var data = "";

	await models.prd_cedulaaplicacion.findAll({
		where: [{
			impresa: 0, estadoid: 2, grupomuestreoid: 3, [sequelize.Op.and]:
				[
					sequelize.literal(`prd_cedulaaplicacion.fechaasignacion > current_date - INTERVAL '12days'`),
				]
		}],
		attributes: ['impresa', 'estadoid', 'semana', 'numerocedula', 'bloqueinterfaceid'],
		include: [{
			model: models.prd_cedulamuestreocalibre, as: 'muestreocal', required: true
		}, {
			model: models.prd_bloqueinterface, as: 'interface', required: true,
			include: [{
				model: models.prd_bloqueciclos, as: 'ciclo', required: true,
				attributes: ["grupodeforza"],
				include: [{
					model: models.prd_bloques, as: 'bloques', required: true,
					attributes: ['numerobloque', 'bloqueid', 'totalsemillas'],
					include: [{
						model: models.prd_lotes, as: 'lotes', required: true,
						attributes: ["loteid", "ciaid", "fcambio", "numerolote"]
					}]
				}]
			}]
		}],

		order: [
			['numerocedula', 'ASC']
		]
	})
		.then(e => {
			data = e;
		})

	return data;
}

/**
 * Actualiza los defectos de la fruta de estimacion
 * @param {cedulamuestreid de la cédula que se está ejecutando en ese momento} defecto
 * @param {cedulamuestreid de la cédula que se está ejecutando en ese momento} cedulamuestreoid
 */
async function actualizarDefectosCedula(cedulamuestreoid, defecto) {

	var { codigoDefecto } = defecto;

	await models.prd_cedmuestreocalrec.findOne({
		where: { cedulamuestreocalibreid: cedulamuestreoid, cod_rechazo_criteriorechazo: codigoDefecto }
	}).then(alerta => {

		if (alerta) {
			alerta.update({
				cantidad: defecto.value,
				semanaestimada: defecto.semanaestimada
			})
		}
	}).catch(
		console.log("Error en actualización de los defectos " + cedulamuestreoid)
	)
}

/**
 * Busca las cédulas de estimación, las actualiza (estadoid=5) a anuladas
 * @param {Cédula capturada del formulario de Kizeo} cedula 
 */
async function AnularCedula(cedula, tipo) {

	try {

		switch (tipo) {
			case "ESTIMACION":

				await models.prd_cedulaaplicacion.findOne({
					where: { numerocedula: cedula },
					include: [{
						model: models.prd_cedulamuestreocalibre, as: 'muestreocal', required: true,
						attributes: ['cedulamuestreocalibreid']
					}],
					order: [
						['cedulaaplicacionid', 'ASC']]
				}).then(alerta => {

					if (alerta) {
						alerta.update({
							estadoid: 5
						})
					}

				})
				break;

			case "PREMADURACION":

				await models.prd_cedulaaplicacion.findOne({
					where: { numerocedula: cedula },
					include: [{
						model: models.prd_cedulamuestreoprema, required: true,
						attributes: ['cedulamuestreopremaid']
					}],
					order: [
						['cedulaaplicacionid', 'ASC']]
				}).then(alerta => {
					if (alerta) {
						alerta.update({
							estadoid: 5
						})
					}

				})

			default:
				break;
		}


	} catch (error) {
		console.log(error);
	}
}

/**
 * Busca las cédulas de estimación, las actualiza (estadoid=3) a ejecutadas y devuelve el cedulamuesteocalibreid
 * @param {Cédula capturada del formulario de Kizeo} cedula 
 */
async function buscarCedula(cedula, tipo) {

	var cedulamuestreocalibreid = 0;

	try {

		switch (tipo) {
			case "ESTIMACION":

				await models.prd_cedulaaplicacion.findOne({
					where: { numerocedula: cedula },
					include: [{
						model: models.prd_cedulamuestreocalibre, as: 'muestreocal', required: true,
						attributes: ['cedulamuestreocalibreid']
					}],
					order: [
						['cedulaaplicacionid', 'ASC']]
				}).then(alerta => {

					if (alerta) {
						cedulamuestreocalibreid = alerta.dataValues.muestreocal.cedulamuestreocalibreid;

						alerta.update({
							estadoid: 3
						})
					}

				})
				break;

			case "PREMADURACION":

				await models.prd_cedulaaplicacion.findOne({
					where: { numerocedula: cedula },
					include: [{
						model: models.prd_cedulamuestreoprema, required: true,
						attributes: ['cedulamuestreopremaid']
					}],
					order: [
						['cedulaaplicacionid', 'ASC']]
				}).then(alerta => {
					if (alerta) {
						cedulamuestreocalibreid = alerta.dataValues.prd_cedulamuestreoprema.cedulamuestreopremaid;

						alerta.update({
							estadoid: 3
						})
					}

				})

			default:
				break;
		}


	} catch (error) {
		console.log(error);
	}

	return cedulamuestreocalibreid;
}
/**
 * Encuentra todos los defectos asignados para los formularios de kizeo en la base de datos y se crea un arreglo con los mismos
 */

async function DefectosFrutaKizeo() {

	var defectoskizeo = [];
	var defecto = {};

	try {
		await models.prd_defectosfruta.findAll({
			where: [{},
			sequelize.where(sequelize.col('kizeo'), Op.ne, '')]
		}).then(
			kizeo => {
				kizeo.map(element => {
					const datos = element.dataValues;

					switch (datos.kizeo) {

						case 'menos':
							defecto = {
								kizeodefecto: 'menos',
								codigo: datos.codigo
							}
							break;
						case 'hueco':
							defecto = {
								kizeodefecto: 'hueco',
								codigo: datos.codigo
							}
							break;
						case 'fallas':
							defecto = {
								kizeodefecto: 'fallas',
								codigo: datos.codigo
							}
							break;
						case 'gomosis':
							defecto = {
								kizeodefecto: 'gomosis',
								codigo: datos.codigo
							}
							break;
						case 'corcho':
							defecto = {
								kizeodefecto: 'corcho',
								codigo: datos.codigo
							}
							break;
						case 'd_sol':
							defecto = {
								kizeodefecto: 'd_sol',
								codigo: datos.codigo
							}
							break;
						case 'desbraq':
							defecto = {
								kizeodefecto: 'desbraq',
								codigo: datos.codigo
							}
							break;
						case 'craw':
							defecto = {
								kizeodefecto: 'craw',
								codigo: datos.codigo
							}
							break;
						case 'fruta_atrazada':
							defecto = {
								kizeodefecto: 'fruta_atrazada',
								codigo: datos.codigo
							}
							break;
						case 'conica':
							defecto = {
								kizeodefecto: 'conica',
								codigo: datos.codigo
							}

							break;
						case 'sombra':
							defecto = {
								kizeodefecto: 'sombra',
								codigo: datos.codigo
							}

							break;

						case 'dano_roedor':
							defecto = {
								kizeodefecto: 'dano_roedor',
								codigo: datos.codigo
							}

							break;
						case 'otros':
							defecto = {
								kizeodefecto: 'otros',
								codigo: datos.codigo
							}

							break;

						case 'techla':
							defecto = {
								kizeodefecto: 'techla',
								codigo: datos.codigo
							}
							break;

						case 'cochinilla':
							defecto = {
								kizeodefecto: 'cochinilla',
								codigo: datos.codigo
							}

							break;

						case 'nat':
							defecto = {
								kizeodefecto: 'nat',
								codigo: datos.codigo
							}
							break;

						default:
							break;
					}
					defectoskizeo.push(defecto);
					defecto = {};
				})
			})
	} catch (error) {
		defectoskizeo = error;
	}
	return defectoskizeo;
}

/**
 * Busca todos los calibres correspondientes a los que se utilizan dentro de los
 * formularios de kizeo dentro de la base de datos y devuelve un array.
 */
async function calibresKizeo() {

	const array_kizeovalues = [];
	var kizeocalibreid = {};

	try {

		await models.mge_calibres.findAll({
			where: [{},
			sequelize.where(sequelize.col('kizeo'), Op.ne, null),
			{
				'kizeo': {
					[Op.ne]: null
				}
			}]
		}
		).then(kizeo => {
			kizeo.map(element => {
				const datos = element.dataValues;

				switch (datos.kizeo) {

					case 's':
						kizeocalibreid = {
							kizeocalibre: 's',
							calibreid: datos.calibreid
						}
						array_kizeovalues.push(kizeocalibreid);
						break;
					case 's1':
						kizeocalibreid = {
							kizeocalibre: 's1',
							calibreid: datos.calibreid
						}
						array_kizeovalues.push(kizeocalibreid);
						break;
					case 's2':
						kizeocalibreid = {
							kizeocalibre: 's2',
							calibreid: datos.calibreid
						}
						array_kizeovalues.push(kizeocalibreid);
						break;
					case 's3':
						kizeocalibreid = {
							kizeocalibre: 's3',
							calibreid: datos.calibreid
						}
						array_kizeovalues.push(kizeocalibreid);
						break;
					case 's4':
						kizeocalibreid = {
							kizeocalibre: 's4',
							calibreid: datos.calibreid
						}
						array_kizeovalues.push(kizeocalibreid);
						break;
					case 's5':
						kizeocalibreid = {
							kizeocalibre: 's5',
							calibreid: datos.calibreid
						}
						array_kizeovalues.push(kizeocalibreid);
						break;
					case 's6':
						kizeocalibreid = {
							kizeocalibre: 's6',
							calibreid: datos.calibreid
						}
						array_kizeovalues.push(kizeocalibreid);
						break;
					case 'sl':
						kizeocalibreid = {
							kizeocalibre: 'sl',
							calibreid: datos.calibreid
						}
						array_kizeovalues.push(kizeocalibreid);
						break;
					case 'sl1':
						kizeocalibreid = {
							kizeocalibre: 'sl1',
							calibreid: datos.calibreid
						}
						array_kizeovalues.push(kizeocalibreid);
						break;
					case 'sl2':
						kizeocalibreid = {
							kizeocalibre: 'sl2',
							calibreid: datos.calibreid
						}
						array_kizeovalues.push(kizeocalibreid);
						break;
					case 'sl3':
						kizeocalibreid = {
							kizeocalibre: 'sl3',
							calibreid: datos.calibreid
						}
						array_kizeovalues.push(kizeocalibreid);
						break;
					case 'sl4':
						kizeocalibreid = {
							kizeocalibre: 'sl4',
							calibreid: datos.calibreid
						}
						array_kizeovalues.push(kizeocalibreid);
						break;

					default:
						break;
				}
			})
		})

	} catch (error) {
		return error
	}

	return array_kizeovalues;
}

/**
 * Busca el calibreid por medio del código de calibre ej. CL5
 * @param {Código de calibre} Calibre 
 */
async function buscarCalibre(Calibre) {

	Calibre = `C${Calibre}`;

	var res = "";

	try {
		await models.mge_calibres.findOne({ where: { codigo: Calibre } })
			.then(element => {
				res = element.calibreid
			})

	} catch (error) {

		res = error;
	}

	return res;
}

/**
 * Se almacenan los detalles de las cédulas por medio de su id de muestreo.
 * @param {id de la cedula muestreo} cedulamuestreodet 
 */
async function grd_CedulaMuestreoDet(cedulamuestreodet) {
	
	var dato = "";
	
	try {
		
		await models.prd_cedmuestreocaldet.create(cedulamuestreodet).then(x => (x));
		dato = "200";

	} catch (error) {
		dato = error;
	}

	return dato;
}

/**
 * Se guardan todos los defectos de la fruta registradas en la cédula o captura desde kizeo
 * @param {Defecto de fruta, objeto literal} defecto 
 */
async function grd_CedulaMuestreoDef(defecto) {

	var dato = "";

	try {
		await models.prd_cedmuestreocalrec.create(defecto).then(x => (x));
		dato = "200";

	} catch (error) {
		dato = error;
	}

	return dato;
}

/**
 * Se almacenan todos los detalles de las cédulas de muestreo de premaduración.
 * @param {detalles de cédula de muestreo, objeto literal} premadet 
 */
async function grd_CedulaPremaDet(premadet) {

	var res = "";

	try {
		await models.prd_cedmuestreopremadet.create(premadet);
		res = "200";

	} catch (error) {
		res = error;
	}

	return res
}

/**
 * Se actualizan los datos nuevos recibidos de kizeo a vistos.
 * @param {Id del formulario al que vamos a actualizar el estado de los datos nuevos} idform 
 * @param {token de acceso para el request} token 
 * @param {arreglo de los id de cada registro de datos} array 
 */
async function datoIngresado(idform, token, array) {

	var msg = "";
	var objectIds = {
		"data_ids":
			array.map(e => (
				e
			))
	};
	var options = {
		url: `https://www.kizeoforms.com/rest/v3/forms/${idform}/markasreadbyaction/test`,
		method: 'POST',
		headers: token,
		body: JSON.stringify(objectIds)
	};

	await request(options, function (err, req, body) {

		console.log(body);
	})

	return msg;
}

/**
 * Las listas externas son divididas mediante un split por medio del símbolo *
 * @param {*} elemento 
 */
function splitArray(e) {

	if (e.fields)
		e = e.fields;

	var arraysplit = e.cedula1 === '' ? [] : e.cedula1.split('*'); //Se obtiene en un arreglo la cedula seleccionada en el combo del formulario de kizeo
	var subarray = arraysplit.length > 0 ? arraysplit[1].split("=") : []; //Se obtiene el número de cédula
	var Numcedula = "";

	if (subarray.length > 0) //Sí se seleccionó la cédulas desde el combo, entra
		Numcedula = subarray[1].trim();
	else
		Numcedula = e.cedula

	return Numcedula;
}


/**
 * Elimina las cédulas duplicadas en una consulta y obtiene el último registrado por la hora
 * @param {Arreglo de datos que contiene lo registrado en Kizeo} arrayObj 
 */
function EliminarDuplicadosCedulas(arrayObj) {

	var cedulasDuplicadas = arrayObj;
	var cedulaFiltrada=[];
	var cedulas = [];

	arrayObj.map(muestreo =>{

		var {cedula} = muestreo.fields;

		cedulaFiltrada = cedulasDuplicadas.filter(ced => ced.fields.cedula === cedula);
		
		if(cedulaFiltrada.length>0){

			cedulaFiltrada.sort(function (a, b) {
				//Se evalua la hora de registro, no la hora que aparece en el formulario
				if (a.fields._create_time > b.fields._create_time) {
					return 1;
				}
				if (a.fields._create_time < b.fields._create_time) {
					return -1;
				}
				return 0;
			});
			
			cedulaFiltrada=cedulaFiltrada.pop(); //Se obtiene el ultimo registro de esa cedula

			cedulas.includes(cedulaFiltrada) ? null : cedulas.push(cedulaFiltrada);
		}
	});

	return cedulas;
}

/**
 * Realiza búsquedas de datos de nuevos desde Kizeo	
 */
async function BuscarRegistrosKizeo() {

	var fecha = new Date();
	var array_cal = [];
	var existenDatos = Object();
	var arraydataids = [];
	var kizeos = [];

	try {
		const token = await getTokenAPI();
		const forms = await getDataAPI(token);
		var array_kizeovalues = await calibresKizeo();
		var array_defectoskizeo = await DefectosFrutaKizeo();

		array_kizeovalues = array_kizeovalues.concat(array_defectoskizeo);

		if (Object.keys(forms)) {
			forms.map(async form => {

				var options = await reqAsync(`https://www.kizeoforms.com/rest/v3/forms/${form.id}/data/unread/test/100? includeupdated`, {
					method: 'GET',
					headers: token
				});

				var data = JSON.parse(options.body).data;

				if (data.length > 0) {
					//Se llena el arreglo de kizeos con los datos recolectados en campo
					await data.map(subE => {
						var formulario = {
							fields: subE,
							id: subE._id
						}
						kizeos.push(formulario);
						arraydataids.push(subE._id); //formulario.id//Este arreglo contiene los id de los formularios a marcar como en visto
					})

					var dataFiltered = await EliminarDuplicadosCedulas(kizeos); 
					kizeos = [];

					dataFiltered.map(async subE => {
						var fields = subE.fields;

						if (fields.cancelar === 'false') {

							if (fields.tipo) {

								switch (fields.tipo) {

									case "ESTIMACION":

										array_cal = array_cal.concat(unirCalibresDatos(fields, array_kizeovalues));
										array_cal.map(async e => {

											existenDatos = await BuscarDetallesCedula(e.cedula, "ESTIMACION");
											

											if (parseInt(existenDatos.cedulamuestreocalibreid) > 0 && existenDatos.conDatos === false) {
												grd_coordenadasEstimacion(existenDatos.cedulamuestreocalibreid, fields);

												if (e.calibreid) {
													const cedulamuestreodet = {
														calibreid: e.calibreid,
														cambiadopor: "gmarin93",
														cantidad: e.valor,
														semanaestimada: e.semanaestimada,
														cantidadnatural: 0,
														cedulamuestreocalibreid: existenDatos.cedulamuestreocalibreid,
														ciaid: 1,
														creadopor: "gmarin",
														fcambio: fecha,
														fcreacion: fecha,
														timestamp: fecha
													}
													await grd_CedulaMuestreoDet(cedulamuestreodet);
												}

												if (e.defectosFruta) {
													e.defectosFruta.map(async defecto => {

														const cedmuestreocalrec = {
															cambiadopor: "gmarin93",
															creadopor: "gmarin",
															fcambio: fecha,
															fcreacion: fecha,
															timestamp: fecha,
															semanaestimada: e.semanaestimada,
															cod_producto_criterio_rechazo: "",
															cod_rechazo_criteriorechazo: defecto.codigoDefecto,
															cod_variedad_criterio_rechazo: "",
															ciaid: 1,
															cantidad: defecto.value,
															cedulamuestreocalibreid: existenDatos.cedulamuestreocalibreid
														}
														await grd_CedulaMuestreoDef(cedmuestreocalrec);
													})
												}
												existenDatos = Object(); //Se limpia el objeto para evitar confusiones de datos
											}
											else {

												if (existenDatos.conDatos) {
													await actualizarDetallesCedula(e, existenDatos.cedulamuestreocalibreid, "ESTIMACION");
													if (e.defectosFruta) {
														e.defectosFruta.map(async defecto => {
															await actualizarDefectosCedula(existenDatos.cedulamuestreocalibreid, defecto);
														})
													}
												}

												if (existenDatos.cedulamuestreocalibreid === 0)
													console.log("Cédula de estimación " + e.cedula + " no encontrada");
											}
											existenDatos = Object();
										})

										array_cal = []; //Limpia el arreglo, ya que cuando se carga cada formulario, el proceso se repite y carga nuevamente el arreglo duplicando todo.
										break;

									case "PREMADURACION":

										var valor = 0;
										var codigoMuestra = "";
										var cochinillaI = 0;
										var cochinillaE = 0;
										existenDatos = false;

										fields.muestro.value.map(async dato => {

											//Se evalua el tipo de muestreo
											if (dato.tipo_de_muestreo.value === 'BORDES') {
												valor = 2;
												codigoMuestra = `B${Math.floor((Math.random() * 10) + 1)}`;
											}
											else if (dato.tipo_de_muestreo.value === 'CENTROS') {
												valor = 1;
												codigoMuestra = `C${Math.floor((Math.random() * 10) + 1)}`;
											}

											//Se evalua si existe cochinilla 
											if (dato.cochinilla_externa.value === 'Sí')
												cochinillaE++;
											else if (dato.cochinilla_interna.value === 'Sí')
												cochinillaI++;

											const calibre = dato.calibre.value
											var calibreid = await buscarCalibre(calibre);

											existenDatos = await BuscarDetallesCedula(fields.cedula.value, "PREMADURACION");
											var premaid = existenDatos.cedulamuestreocalibreid > 0 ? existenDatos.cedulamuestreocalibreid : false

											if (!existenDatos.conDatos) {

												await actualizarCochinilla(cochinillaI, cochinillaE, premaid);
												cochinillaI = 0;
												cochinillaE = 0;


												await grd_coordenadasPremaduracion(existenDatos.cedulamuestreocalibreid, fields);

												const cedulapremadet = {

													brix: parseFloat(dato.brix.value),
													calibreid: calibreid,
													cambiadopor: 'gmarin93',
													cedulamuestreopremaid: existenDatos.cedulamuestreocalibreid,
													ciaid: 1,
													codigomuestra: codigoMuestra,
													creadopor: 'gmarin93',
													fcambio: fecha,
													fcreacion: fecha,
													nivelib: dato.ib1.value === '' ? 0 : dato.ib1.value,
													timestamp: fecha,
													tipomuestra: valor,
													translucidez: parseFloat(dato.translucidez.value)
												}

												await grd_CedulaPremaDet(cedulapremadet);
											}
											else {

												const cedulapremadet = {

													brix: parseFloat(dato.brix.value),
													calibreid: calibreid,
													nivelib: dato.ib1.value === '' ? 0 : dato.ib1.value,
													translucidez: dato.translucidez.value === '' ? 0 : parseFloat(dato.translucidez.value)
												}
												actualizarDetallesCedula(cedulapremadet, premaid, "PREMADURACION");
											}
										})
										break;

									default:
										break;
								}
							}

						}
						// else if (fields.cancelar.value === 'Sí') {
						else if (fields.cancelar === 'true') {

							var Numcedula = splitArray(fields);

							const arr = [subE.id];
							await datoIngresado(form.id, token, arr);
							await AnularCedula(Numcedula, subE.fields.tipo);
							obtenerListasExternas(); //Se actualiza la lista externa
						}

					})
					// Actualiza los datos a leidos
					await datoIngresado(form.id, token, arraydataids);
				}
				// });
			})
		}
	} catch (error) {
		console.log(error);
	}
}

/*se obtiene toda la información que se carga en la tabla el primer método obtiene el token 
para la autorización del header que sera utilizada en el segundo método(formDetail) para realizar el request.
*/
router.get('/getData/:id', getToken, formDetail, function (req, res, next) {
	res.format({
		json: function () {
			res.json({
				data: res.locals.data
			});
		}
	});
});


/*este método me obtiene los detalles de un formulario en especifíco */
function formDetail(req, res, next) {
	var formid = req.params.id;

	var options = {
		url: `https://www.kizeoforms.com/rest/v3/forms/${formid}/data/readnew`,
		method: 'GET',
		headers: res.locals.token
	};

	request(options, function (err, req, body) {
		var json = JSON.parse(body);
		res.locals.data = json;
		next();
	});
}

async function formDetailId(id, token) {
	var formid = id;
	var jsonData = "";

	var options = {
		url: `https://www.kizeoforms.com/rest/v3/forms/${formid}/data/readnew`,
		method: 'GET',
		headers: token
	};

	request(options, function (err, req, body) {
		jsonData = JSON.parse(body);
	});

	return jsonData;
}

/*este método me obtiene el id del formulario */
function getData(req, res, next) {
	var options = {
		url: 'https://www.kizeoforms.com/rest/v3/forms',
		method: 'GET',
		headers: res.locals.token
	};

	/*librería de node js para realizar la solicitud */
	request(options, function (err, req, body) {
		var json = JSON.parse(body);
		// res.locals.id = json.forms[0].id;
		res.locals.forms = json.forms;
		next();
	});
}

/*este método me obtiene el id del formulario */
async function getDataAPI(token) {

	var forms = "";

	try {

		forms = await reqAsync('https://www.kizeoforms.com/rest/v3/forms', {
			method: 'GET',
			headers: token
		});

		forms = (JSON.parse(forms.body).forms);

	} catch (error) {
		console.log(error);
	}

	return forms;
}


/*método para construir el formato del header */
function header(token) {
	var header = {};

	header = {
		Authorization: token,
		'Content-Type': 'application/json'
	};
	return header;
}

/*método que me obtiene y me guarda el token */
function getToken(req, res, next) {
	var myJSONObject = {
		user: 'lalydia',
		password: 'Lydia2019',
		company: 'AGRICO4'
	};

	request({
		url: 'https://www.kizeoforms.com/rest/v3/login',
		method: 'POST',
		json: true, // <--Very important!!!
		body: myJSONObject
	},
		async function (error, response, body) {
			saveObject = {
				users: myJSONObject.user,
				password: myJSONObject.password,
				token: response.body.data.token
			};
			res.locals.token = header(response.body.data.token);
			next();
		}
	);
}

/*método que me obtiene y me guarda el token */
async function getTokenAPI() {

	var myJSONObject = {
		user: 'lalydia',
		password: 'Lydia2019',
		company: 'AGRICO4'
	};

	var token = "";

	try {

		token = await reqAsync('https://www.kizeoforms.com/rest/v3/login', {
			method: 'POST',
			data: myJSONObject
		});

		if (token.length != "")
			token = header(JSON.parse(token.body).data.token);

	} catch (error) {
		console.log(error);
	}
	return token
}

app.listen(4000, async function () {

	try {
		console.log('Obteniendo datos del API de Kizeo ...');
		//Se obtienen los datos cuando se abre la página
		await BuscarRegistrosKizeo();
		await obtenerListasExternas();
		//Se ejecuta al iniciar el archivo y obtener los datos de la estación
		setInterval(async function () {

			let fechaActual = new Date();

			try {

				if (fechaActual.getHours() === 0 || fechaActual.getHours() < 6) {

					if (fechaActual.getHours() < 6)
						console.log("Consultas suspendidas hasta las 6:00 am " + fechaActual);


				}
				else {
					console.log("Consultas reanudadas " + fechaActual)
					await BuscarRegistrosKizeo();
				}


			} catch (error) {
				console.log(error);
			}
			// }, 5000);
		}, 600000);//900000); //15 min

		//Cada 24 horas actualiza las cédulas pendientes por muestreo
		setInterval(async function () {
			await obtenerListasExternas();
		}, 86400000);
	} catch (error) {
		console.log("Error en consulta al API de kizeo")
		console.log(error);
	}
});

// module.exports = router;	