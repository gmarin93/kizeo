'use strict';
module.exports = (sequelize, DataTypes) => {
  var cedmuestreocalrec = sequelize.define('prd_cedmuestreocalrec', {
  
cambiadopor:DataTypes.STRING,
ciaid:DataTypes.INTEGER,
creadopor:DataTypes.STRING,
cod_producto_criterio_rechazo:DataTypes.STRING,
cod_rechazo_criteriorechazo:DataTypes.INTEGER,
cod_variedad_criterio_rechazo:DataTypes.STRING,
cantidad:DataTypes.INTEGER,
cedmuestreocalrechazoid:{type:DataTypes.INTEGER, primaryKey:true,autoIncrement: true},
cedulamuestreocalibreid:DataTypes.INTEGER,
porcentaje:DataTypes.INTEGER,
semanaestimada:DataTypes.INTEGER,
fcambio :{type:DataTypes.DATE, updatedAt:true},
fcreacion :{type:DataTypes.DATE, createdAt:true},
timestamp: {type:DataTypes.DATE, createdAt:true},


  //Con timestamps en false, no toma en cuenta la columna creatAt y UpdateAt ya que
  // la tabla prd_bloques no posee dicha columna 
  //Dentro de esas llaves se especifican los options de los modelos
  }, {timestamps: false,freezeTableName: true});
  cedmuestreocalrec.associate = function(models) {
  }


  return cedmuestreocalrec;
};
