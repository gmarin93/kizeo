'use strict';
module.exports = (sequelize, DataTypes) => {
  var lote = sequelize.define('prd_lotes', {
    anioscontrato: DataTypes.INTEGER,
    cambiadopor: DataTypes.STRING,
    centrocostoid: DataTypes.INTEGER,
    ciaid: DataTypes.INTEGER,
    creadopor: DataTypes.STRING,
    cuentainversionid: DataTypes.INTEGER,
    descripcion: DataTypes.STRING,
    estado:DataTypes.INTEGER,
    fcambio:DataTypes.TIME,
    fcreacion: DataTypes.TIME,
    fechafinalcronograma: DataTypes.TIME,
    fechafincontrato: DataTypes.TIME,
    fechainiciocontrato: DataTypes.TIME,
    fincaid: DataTypes.INTEGER,
    hectareas: DataTypes.DECIMAL,
    hectareasbrutas: DataTypes.DECIMAL,
    loteid:{type:DataTypes.INTEGER, primaryKey:true},
    mapa: DataTypes.STRING,
    nombrelote: DataTypes.STRING,
    numerolote: DataTypes.STRING,
    preciohectarea: DataTypes.INTEGER,
    propietario: DataTypes.STRING,
    sectorid: DataTypes.INTEGER,
    valoralquiler: DataTypes.INTEGER,
    paquetetecnologicoid: DataTypes.INTEGER,
    canalesprimarios: DataTypes.INTEGER,
    canalessecundarios: DataTypes.INTEGER,
    anulado: DataTypes.INTEGER,
    anuladopor: DataTypes.STRING,
    fanulacion: DataTypes.TIME,
    aniolote: DataTypes.INTEGER,

  }, {timestamps: false});

  lote.associate = function(models) {

    lote.hasMany(models.prd_bloques, {   
      foreignKey:'loteid',
      as: 'lotes'
     });

  };

  return lote;
};