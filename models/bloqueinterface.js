
'use strict';
module.exports = (sequelize, DataTypes) => {
  var bloque = sequelize.define('prd_bloqueinterface', {

    anioforzamiento:DataTypes.INTEGER,
    bloquecicloid:DataTypes.INTEGER,
    bloqueinterfaceid:{type:DataTypes.INTEGER, primaryKey:true},
    cambiadopor:DataTypes.STRING,
    ciaid:DataTypes.INTEGER,
    creadopor:DataTypes.STRING,
    estadonatural:DataTypes.INTEGER,
    fcambio:{type:DataTypes.DATE, updatedAt:true},
    fcreacion:DataTypes.DATE,
    id_cosecha:DataTypes.INTEGER,
    id_division:DataTypes.INTEGER,
    id_ucp:DataTypes.INTEGER,
    lote:DataTypes.STRING,
    porcentajerechazo:DataTypes.INTEGER,
    porcentajeforza:DataTypes.INTEGER,
    productividadestimada:DataTypes.INTEGER,
    seccion:DataTypes.INTEGER,
    semana_siembra:DataTypes.INTEGER,
    semanaforzamiento:DataTypes.INTEGER,
    year_siembra:DataTypes.INTEGER,


  //Con timestamps en false, no toma en cuenta la columna creatAt y UpdateAt ya que
  // la tabla prd_bloques no posee dicha columna 
  //Dentro de esas llaves se especifican los options de los modelos
  }, {timestamps: false, freezeTableName: true,});
  bloque.associate = function(models) {
    
    bloque.belongsTo(models.prd_bloqueciclos, {   
      foreignKey:'bloquecicloid', targetKey:'bloquecicloid',as: 'ciclo'
     });
  };


  return bloque;
};
