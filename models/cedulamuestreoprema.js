'use strict';
module.exports = (sequelize, DataTypes) => {
  var cedulamuestreoprema  = sequelize.define('prd_cedulamuestreoprema', {
  
cambiadopor:DataTypes.STRING,
cedulaaplicacionid:DataTypes.INTEGER,
cedulamuestreopremaid:{type:DataTypes.INTEGER, primaryKey:true,autoIncrement: true},
ciaid:DataTypes.INTEGER,
creadopor:DataTypes.STRING,
distancia:DataTypes.INTEGER,
fcambio :{type:DataTypes.DATE, updatedAt:true},
fcreacion :{type:DataTypes.DATE, createdAt:true},
muestreadorid:DataTypes.INTEGER,
cantidadmuestrasbordes:DataTypes.INTEGER,
cantidadmuestrascentros:DataTypes.INTEGER,
timestamp: {type:DataTypes.DATE, createdAt:true},
promediobrix:DataTypes.INTEGER,
promedioib:DataTypes.INTEGER,
promediotranslucidez:DataTypes.INTEGER,
timestamp:DataTypes.INTEGER,
totalcochinillaexterna:DataTypes.INTEGER,
totalcochinillainterna:DataTypes.INTEGER,
gpsinicial:DataTypes.STRING,
gpsfinal:DataTypes.STRING,


  //Con timestamps en false, no toma en cuenta la columna creatAt y UpdateAt ya que
  // la tabla prd_bloques no posee dicha columna 
  //Dentro de esas llaves se especifican los options de los modelos
  }, {timestamps: false,freezeTableName: true});
  cedulamuestreoprema.associate = function(models) {
    
  };

  return cedulamuestreoprema;
};
