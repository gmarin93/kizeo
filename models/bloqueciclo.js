'use strict';
module.exports = (sequelize, DataTypes) => {
  var bloque = sequelize.define('prd_bloqueciclos', {

    anio:DataTypes.INTEGER,
    bloquecicloetapaidactual:DataTypes.INTEGER,
    bloquecicloid:{type:DataTypes.INTEGER, primaryKey:true},
    bloqueid:DataTypes.INTEGER,
    cambiadopor:DataTypes.STRING,
    ciaid:DataTypes.INTEGER,
    codigofasetss:DataTypes.STRING,
    creadopor:DataTypes.STRING,
    damproyectado:DataTypes.INTEGER,
    estado:DataTypes.INTEGER,
    fcambio:{type:DataTypes.DATE, updatedAt:true},
    fcreacion:DataTypes.DATE,
    fechaforza:DataTypes.DATE,
    fechamaduracion:DataTypes.DATE,
    fechacierre:DataTypes.DATE,
    fechainciociclo:DataTypes.DATE,
    fechasemilla:DataTypes.DATE,
    grupodeforza:DataTypes.STRING,
    grupoid:DataTypes.INTEGER,
    hectareascosechadas:DataTypes.INTEGER,
    numerociclo:DataTypes.INTEGER,
    perdidaotrascausas:DataTypes.INTEGER,
    periodo:DataTypes.INTEGER,
    pesoplanta:DataTypes.INTEGER,
    plantascosechadas:DataTypes.INTEGER,
    semana:DataTypes.INTEGER,
    tipoplantacionid:DataTypes.INTEGER,


  //Con timestamps en false, no toma en cuenta la columna creatAt y UpdateAt ya que
  // la tabla prd_bloques no posee dicha columna 
  //Dentro de esas llaves se especifican los options de los modelos
  }, {timestamps: false});
  bloque.associate = function(models) {
 
    bloque.hasOne(models.prd_bloques, {   
      foreignKey:'bloquecicloactualid',
      as:'bloques'
     });

    bloque.belongsTo(models.prd_bloqueinterface, {   
      foreignKey:'bloquecicloid',
      as:'ciclo'
     });
  };
    


  return bloque;
};
