'use strict';
module.exports = (sequelize, DataTypes) => {
  var bloque = sequelize.define('prd_bloques', {
  
  alturanivelmarid:DataTypes.INTEGER,
  bloquecicloactualid:DataTypes.INTEGER,
  bloqueid:{type:DataTypes.INTEGER, primaryKey:true},
  cambiadopor:DataTypes.STRING,
  canalessecundarios:DataTypes.INTEGER,
  canalesterciarios:DataTypes.INTEGER,
  cantidadcamaid:DataTypes.INTEGER,
  centrocostoid:DataTypes.INTEGER,
  ciaid:DataTypes.INTEGER,
  codigodensidadtss:DataTypes.STRING,
  codigofasetss:DataTypes.STRING,
  creadopor:DataTypes.STRING,
  cuentainversionid:DataTypes.INTEGER,
  densidad:DataTypes.INTEGER,
  densidadid:DataTypes.INTEGER,
  estado:DataTypes.INTEGER,
  etapaid:DataTypes.INTEGER,
  familiaid:DataTypes.INTEGER,
  fcambio:{type:DataTypes.DATE, updatedAt:true},
  fcreacion:DataTypes.DATE,
  fechamaduracion:DataTypes.DATE,
  fechacierre:DataTypes.DATE,
  fechafinalcronograma :DataTypes.DATE,
  fechaforza:DataTypes.DATE,
  fechasemilla:DataTypes.DATE,
  fechasiembra:DataTypes.DATE,
  fincaid:DataTypes.INTEGER,
  grupoforzaid:DataTypes.INTEGER,
  grupodeforza:DataTypes.STRING,
  hectareas:DataTypes.INTEGER,
  hectareasbrutas:DataTypes.INTEGER,
  loteid:DataTypes.INTEGER,
  loteprocedenciasemilla:DataTypes.STRING,
  materialsemillaidtss:DataTypes.INTEGER,
  matsemid:DataTypes.INTEGER,
  numerobloque:DataTypes.INTEGER,
  ordenprdid:DataTypes.INTEGER,
  pesoplanta:DataTypes.INTEGER,
  tamaniosemillaid:DataTypes.INTEGER,
  tamanosiembraidtss:DataTypes.STRING,
  tipocamaid:DataTypes.INTEGER,
  totalsemillas:DataTypes.INTEGER,
  variedadid:DataTypes.INTEGER,
  paquetetecnologicoid:DataTypes.INTEGER,
  anulado:DataTypes.INTEGER,
  anuladopor:DataTypes.STRING,
  fanulacion :DataTypes.DATE,
  fechafinavancesiembra:DataTypes.DATE,
  esorganico:DataTypes.INTEGER,
  bloquecicloactualid:DataTypes.INTEGER

  //Con timestamps en false, no toma en cuenta la columna creatAt y UpdateAt ya que
  // la tabla prd_bloques no posee dicha columna 
  //Dentro de esas llaves se especifican los options de los modelos
  }, {timestamps: false});
  bloque.associate = function(models) {
 
    bloque.belongsTo(models.prd_lotes,{
      foreignKey:'loteid',
      as: 'lotes'
    });

  };
    


  return bloque;
};
