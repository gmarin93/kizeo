'use strict';
module.exports = (sequelize, DataTypes) => {
  var cedulapremadet = sequelize.define('prd_cedmuestreopremadet', {
  
cambiadopor:DataTypes.STRING,
cedulamuestreopremaid:DataTypes.INTEGER,
ceudlamuestreocalibrepremaid:{type:DataTypes.INTEGER, primaryKey:true,autoIncrement: true},
ciaid:DataTypes.INTEGER,
creadopor:DataTypes.STRING,
cambiadopor:DataTypes.STRING,
fcambio :{type:DataTypes.DATE, updatedAt:true},
fcreacion :{type:DataTypes.DATE, createdAt:true},
nivelib:DataTypes.INTEGER,
timestamp: {type:DataTypes.DATE, createdAt:true},
calibreid:DataTypes.INTEGER,
brix:DataTypes.INTEGER,
codigomuestra:DataTypes.STRING,
tipomuestra:DataTypes.INTEGER,
translucidez:DataTypes.INTEGER
 
  //Con timestamps en false, no toma en cuenta la columna creatAt y UpdateAt ya que
  // la tabla prd_bloques no posee dicha columna 
  //Dentro de esas llaves se especifican los options de los modelos
}, {timestamps: false,freezeTableName: true});
cedulapremadet.associate = function(models) {
  
};

return cedulapremadet;
};
