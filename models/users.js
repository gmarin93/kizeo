'use strict';
module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    users: DataTypes.STRING,
    password: DataTypes.STRING,
    token: DataTypes.STRING
  }, {});
  users.associate = function(models) {
    // associations can be defined here
  };
  return users;
};