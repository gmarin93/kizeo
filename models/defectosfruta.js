'use strict';
module.exports = (sequelize, DataTypes) => {
  var defectosfruta = sequelize.define('prd_defectosfruta', {
  
cambiadopor:DataTypes.STRING,
estado:DataTypes.INTEGER,
defectoid:{type:DataTypes.INTEGER, primaryKey:true,autoIncrement: true},
ciaid:DataTypes.INTEGER,
creadopor:DataTypes.STRING,
codigo:DataTypes.STRING,
fcambio :{type:DataTypes.DATE, updatedAt:true},
fcreacion :{type:DataTypes.DATE, createdAt:true},
descripcion:DataTypes.STRING,
timestamp: {type:DataTypes.DATE, createdAt:true},
descripcionlarga:DataTypes.STRING,
categoriarechazoid:DataTypes.INTEGER,
peso:DataTypes.STRING,
kizeo:DataTypes.STRING

  //Con timestamps en false, no toma en cuenta la columna creatAt y UpdateAt ya que
  // la tabla prd_bloques no posee dicha columna 
  //Dentro de esas llaves se especifican los options de los modelos
  }, {timestamps: false,freezeTableName: true});
  defectosfruta.associate = function(models) {
    
    // cedulaaplicacion.hasMany(models.prd_bloquemapaestilo, {   
    //   foreignKey:'bloqueid'
    //  });

    //  cedulaaplicacion.belongsTo(models.prd_lotes,{
    //   foreignKey:'loteid'
    // });

  };
    
//   cedulaaplicacion.get= function(loteid){
    
//     return sequelize.query(
//       ' select split_part(to_char(numerobloque,'+"'999D99S'"+'),'+"','"+',1) as numbloque,split_part(to_char(numerobloque,'+"'999D99'"+'),'+"','"+',2) as seccionbloque'+
//       ',bloc.hectareas,bloc.totalsemillas,bloc.densidad,gr.codigo as grupo,ts.codigo as material,'+
//        'et.descripcion as etapa,date_part('+"'month'"+',(fechasiembra)) as mes, date_part('+"'day'"+',(fechasiembra)) as dia,date_part('+"'year'"+',(fechasiembra)) as anio,bloc.bloqueid,lo.loteid from prd_bloques bloc '+
//       ' inner join prd_bloqueciclos ciclo on ciclo.bloquecicloid = bloc.bloquecicloactualid '+
//       ' inner join prd_bloquecicloetapa etap on etap.bloquecicloetapaid = ciclo.bloquecicloetapaidactual '+
//       ' inner join prd_etapas et on et.etapaid=etap.etapaid'+
//       ' inner join prd_tamaniosemilla ts on ts.tamaniosemillaid=bloc.tamaniosemillaid'+
//       ' inner join prd_lotes as lo on lo.loteid=bloc.loteid'+
//       ' inner join est_grupos gr on gr.grupoid=ciclo.grupoid'+
//       ' where lo.numerolote='+"'"+loteid+"'"+'order by bloc.bloqueid',{ type: sequelize.QueryTypes.SELECT}
//     );
//   };

//   bloque.tierraBloqueEtapa= function(){
    
//     return sequelize.query(

      
// 	  'select sum(pb.hectareas) as tierra,et.descripcion as etapa,bce.etapaid  from prd_bloquecicloetapa bce'+
//       ' inner join prd_bloqueciclos bc on bc.bloquecicloetapaidactual=bce.bloquecicloetapaid' +
//       ' inner join prd_etapas et on et.etapaid=bce.etapaid ' + 
//       ' inner join prd_bloques pb on pb.bloquecicloactualid = bc.bloquecicloid ' +  
//       ' group by bce.etapaid,et.descripcion ' , 
         
//    { type: sequelize.QueryTypes.SELECT}
//     );
//   };

//   bloque.tierraBloqueEtapaLote= function(loteid,anio){
    
//     return sequelize.query(
      
//       'select distinct bce.etapaid,sum(pb.hectareas) as tierra,bc.estado,bc.bloqueid,bc.bloquecicloid,et.descripcion as etapa, pb.numerobloque' + 
//         ' from prd_bloquecicloetapa bce' +
//          ' inner join prd_bloqueciclos bc on bc.bloquecicloetapaidactual=bce.bloquecicloetapaid ' +
//          ' inner join prd_etapas et on et.etapaid=bce.etapaid ' +
//          ' inner join prd_bloques pb on pb.bloquecicloactualid = bc.bloquecicloid ' + 
//          ' inner join prd_lotes lo on lo.loteid=pb.loteid ' + 
//          ' where lo.numerolote='+"'"+loteid+"'"+
//          ' and extract(year from bce.fcreacion)=' +"'"+anio+"'"+ 'and lo.estado=1'+
//          ' group by bc.estado,pb.bloqueid,et.descripcion,bc.bloqueid,bc.bloquecicloid,bce.etapaid,bce.fcreacion,lo.numerolote ' , 

//        { type: sequelize.QueryTypes.SELECT}
//     );
//   };
  

//   bloque.historial= function(anio){
    
//     return sequelize.query(
      

//       ' select sum(pb.hectareas) as tierra,et.descripcion as etapa,bc.anio  from prd_bloquecicloetapa bce ' + 
//          ' inner join prd_bloqueciclos bc on bc.bloquecicloetapaidactual=bce.bloquecicloetapaid ' +
//          ' inner join prd_etapas et on et.etapaid=bce.etapaid ' + 
//          ' inner join prd_bloques pb on pb.bloquecicloactualid = bc.bloquecicloid ' + 
//          ' where bc.anio='+"'"+anio+"'"+
//          ' group by bce.etapaid,et.descripcion,bc.anio' ,
//        { type: sequelize.QueryTypes.SELECT}
//     );
//   };
  
//   bloque.bloques= function(numerolote){
//     return sequelize.query(
//       'select bl.numerobloque as numbloque,bl.bloqueid,lo.loteid from prd_bloques bl'+
//      ' inner join prd_lotes lo on lo.loteid=bl.loteid'+
//      '  where lo.numerolote like' +"'"+numerolote+"'",
//        { type: sequelize.QueryTypes.SELECT}
//     );
//   };
  



  return defectosfruta;
};
